Source: libpdl-netcdf-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Henning Glawe <glaweh@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-pdl,
               libdevel-checklib-perl,
               libnetcdf-dev,
               pdl (>= 1:2.043),
               perl-xs-dev,
               perl:native
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libpdl-netcdf-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libpdl-netcdf-perl.git
Homepage: https://metacpan.org/release/PDL-NetCDF
Rules-Requires-Root: no

Package: libpdl-netcdf-perl
Architecture: any
Depends: ${pdl:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: Netcdf-IO for PDL
 PDL::NetCDF provides the PDL interface to the Unidata NetCDF library.
 It uses the netCDF version 3 library to make a subset of netCDF functionality
 available to PDL users in a clean, object-oriented interface.
 .
 The NetCDF standard allows N-dimensional binary data to be efficiently
 stored, annotated and exchanged between many platforms.
 .
 When one creates a new netCDF object, this object is associated with one
 netCDF file.
